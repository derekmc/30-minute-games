# Rock Paper Scissors Asynchronous

## Task

 1. Replace the built-in browser functions "alert" and "prompt",
    with custom asychronous ones: "alertAsync", and "promptAsync".

 2. Replace the calls to "alert" and "prompt" in index.html with calls to 
    the asynchronous versions.

## Rules

 1. Do not use any external dependencies or libraries for this project.

 2. Keep all code in a single file, do not factor this project's code.

## Learning goals

 - Programming with state.
 - Browser Built-In functions: alert, prompt, and confirm.
 - Simple html UI inputs with no libraries or dependencies.
 - Asynchronous callback programming.

## Full Project Description

Browsers have 3 legacy built-in functions that are useful for quick and dirty
development or learning, but look extremely ugly, and may not be supported
in the future. Notably, all 3 of these functions are *synchronous* which
means that the control flow of your program will stop(hammer time) in place
waiting for the function to complete, which in this case involves waiting
for the user.

 - alert(message) // Display a message and wait for user.
 - prompt(message) // Asks the user for text input.
 - confirm(message) // Asks the user a binary question (ok, cancel).

Using these 3 functions, it is easy to write simple programs or games,
as I have done here with "Rock, Paper, Scissors".

This project is simply to replace the bult-in versions of "alert()" and "confirm()"
with custom asynchronous ones.

Some potential helper or example code is provided below

### Helper/Example Code:

    // Example of how to use async replacements
    function testAsyncBuiltin(){
        promptAsync("What is your name?", function(name){
                alertAsync("Hello, " + name, function(){
                    console.log("all done"); 
                })
            }, function(){
                alertAsync("Nevermind.");
            }
        )
    }
    // dialog templates
    var alert_html =
        "<div><p>{{message}}</p>" + 
        "<button onclick='alertFinish();'>Ok</button>" +
        "</div>";

    var prompt_html =
        "<div><p>{{message}}</p>" +
        "<input type='text' id='prompt_input'></input><br>" +
        "<button onclick='promptFinish()'>Ok</button>" +
        "<button onclick='promptCancel()'>Cancel</button>" +
        "</div>";

    // example of how to use the template
    // note this is insecure as the html is not escaped.
    // document.body.innerHTML = alert_html.replace("{{message}}", "custom message goes here");



    // store prompt or alert callbacks in these variables
    var alert_next;
    var prompt_next;
    var prompt_cancel;

    function alertAsync(message, next){
        // create a dialog style message
        // and then call next()
    }
    function promptAsync(message, next, cancel){
        // create UI elements to get an input from the user,
        // and call next(input), when they click done.
    }
    function alertFinish(){
        // called when alert is finished.
    }
    function promptFinish(){
        // okay was clicked.
    }
    function promptCancel(){
        // cancel was clicked.
    }
